# pack_cli

### 介绍

本项目基于laya游戏引擎，请遵循laya官方的建议，在游戏中标注以下内容。
```
在游戏加载页或首页的任意可见位置注明：Powered by LayaAir Engine
```
游戏引擎编写不易，打造生态更难，大家都多多支持laya官方。

layapp工具QQ交流群:457800610

#### 打包方面

依赖json配置，使用命令行来打包 wx，oppo，qq，web等，注意，这里打的包，必需是干净的包，不包含各个平台的相关代码。

如果想要一份代码打所有的平台的话，必需开发者自己定义好自己的库，可以用来适配所有的平台。

#### 创建laya项目

本项目创建的是使用laya作为引擎的空项目。laya(http://layabox.com/)。当前使用的是2.4.0的正式版本。随时更新最新正式版。

tsconfig中，使用了es6，编译使用了rollup，注意代码编写，最好不要出现循环引用，不然编译会报警告。但是可以编译通过。

默认使用了fairyGUI作为UI框架，需要使用者安装fairyGUI(https://www.fairygui.com/)。当然，自己爱用什么都可以~

### [更新列表](./layapp/CHANGELOG.md)

### 计划

- [ ] 打包oppo小游戏时,去掉自行安装quickgame-toolkit这一步
- [x] 打包头条小游戏
- [x] 打包web游戏


### 已完成

- 创建laya引擎项目. `layapp cp myproj [2.4.0]`,版本为可选项,默认2.4.0,laya引擎正式版本更新后跟着更新,创建了可运行的模板项目
- wx小游戏打包(默认使用了laya的引擎插件). `layapp dev wx` or `layapp prod wx`
- oppo小游戏打包(需要自行安装quickgame-toolkit<设置好全局变量> 和 openssl(生成好正式签名)). `layapp d oppo` or `layapp p oppo`
- vivo小游戏打包(需要自行安装 openssl(生成好正式签名)). `layapp d vivo` or `layapp p vivo`

### 安装教程

1.  安装nodejs v8.9以上 或者 nodejs v10
2.  `npm i layapp -g`
3.  在命令提示符中输入：`layapp version`(如果出现version: x.x.x, 则说明安装成功)


### 使用说明

#### 打包

1.  在laya的ide中编译完毕,出现bin目录(无论什么方式编译完毕，只要有可运行的bin目录即可)
2.  在项目中: `layapp init [项目名] [平台]`
3.  在打包项目的json中，配置好对应项目的打包配置,如: `layapp-projectname-wx.json`, `layapp-projectname-qq.json`
4.  打包: `layapp [dev,prod] [指定的打包配置平台(多个配置的情况下需要，默认不填是wx)]` eg: `layapp d wx` or `layapp dev wx,oppo` or `layapp d all` or `layapp d`
5.  注意：oppo和vivo打包，没有支持分包，等待后续开发

#### 配置详情

#### 打包时需要注意,库文件要在各自的index.js中定义好

layapp-projectname-wx.json

```
{
    "init": {
        "plat": "${PLAT}", /** "web", "qq", "tt", "oppo", ... */
        "templatePath": "", /**不填则调用软件自带的模板*/
        "appid": "wxasfsdfksd123123", /**即微信或者其他平台的appid*/
        "projectName": "${PROJECT_NAME}" /**项目名*/
    },
    "dev": { /**开发版本的配置*/
        "packageOptions": { /**打包的配置选项*/
            "projectBin": "D:/xxxx/", /** 绝对路径 打包时的来源目录,一般是bin目录*/
            "outputPath": "D:/publish/", /** 绝对路径 导出对应的平台项目目录*/
            "resPath": ["res", "resfirst"], /** 资源在bin下的目录名列表,要拷贝到minigame中的,也可以是文件 */
            "resVer": "1.0.0", /**生成cdn资源目录,做资源版本管理用,生成的目录和wxgame同级,为空则直接拷贝res到minigame中 */
            "skipResCheck": true, /** 跳过资源检查,即:不拷贝bin中的素材到指定目录 */
            "skipResHash": true, /**(未实现)跳过资源的hash重命名 */
            "skipCodeUglify": true, /**(未实现)跳过代码压缩*/
            "enginePlugin": true, /** (只实现了插件2.3.0)是否使用引擎插件,当前默认使用插件2.3.0 */
            "es6toes5": false /** (未实现)不推荐使用! 代码是否要把es6转到se5, 因为微信支持ios9,laya引擎如果是2.2以后,且准备支持ios9的,需要true */
        },
        "config": {/** 这个config是具体游戏中需要的配置，wx项目里会挂在window['config']下面 */
            "cdn": "https://xxx.xx.xxx/1.0.0/", /** 注意后面的/ , 这个地方也可以不需要1.0.0, 不过要在项目代码中自定义, (cdn + ver + '/')作为组合 */
            "debug": true, /** 项目中是否显示输出和是否有debug方法 */
            "ver": "1.0.0" /**这个(ver 1.0.0)要和packageOptions中的resVer保持一致*/
        },
        "packageOverScript": "D:/xxx/dev.bat 1.0.0" /** 1.0.0 为参数; 最后不要加pause; 打包完成后是否需要自定义脚本,分包或者git提交的逻辑一般在这里处理 */
    },
    "prod": { /** 正式版本的配置*/
        "packageOptions": {
            "projectBin": "D:/xxxx/",
            "outputPath": "D:/publish/",
            "resPath": ["res", "resfirst"],
            "resVer": "1.0.0",
            "skipResCheck": false,
            "skipResHash": true,
            "skipCodeUglify": false,
            "enginePlugin": true,
            "es6toes5": false
        },
        "config": {
            "cdn": "https://xxx.xx.xxx/1.0.0/",
            "debug": false,
            "ver": "1.0.0"
        },
        "packageOverScript": "D:/xxx/prod.bat"
    }
}

```

layapp-proj-oppo.json

```
{
    "init": {
        "plat": "${PLAT}", /** "web", "qq", "tt", "oppo", ... */
        "templatePath": "", /**不填则调用软件自带的模板*/
        "appid": "", /**即微信或者其他平台的appid*/
        "projectName": "${PROJECT_NAME}" /**项目名*/
    },
    "dev": { /**开发版本的配置*/
        "packageOptions": { /**打包的配置选项*/
            "packageId": "com.xx.xx", /** oppo中的manifest.json中的package的名字定义 */
            "gameName": "游戏名字", /** oppo中的manifest.json中的name的名字定义 */
            "versionName": "1.0.0", /** oppo中的manifest.json中的版本号定义，每次打正式的时候都要修改，测试无所谓 */
            "versionCode": 1, /** oppo中的manifest.json中的版本数字定义，每次打正式的时候都要修改，测试无所谓 */
            "minPlatformVersion": 1050, /** oppo中的manifest.json中的最小平台版本号定义 */
            "iconPath": "D:/xxx/logo.png", /** oppo中的manifest.json中的icon定义 */
            "orientation": "portrait", /** oppo中的manifest.json中的横竖屏定义，value:landscape|portrait */
            "signPath": "", /** oppo中打正式包需要的签名文件的路径,dev模式下可以为空，prod需要填，签名需要自己下载opensll自己生成 */
            "projectBin": "D:/xxxx/", /** 绝对路径 打包时的来源目录,一般是bin目录*/
            "outputPath": "D:/publish/", /** 绝对路径 导出对应的平台项目目录*/
            "resPath": [
                "res",
                "resfirst"
            ], /** 资源在bin下的目录名列表,要拷贝到minigame中的 */
            "resVer": "", /**生成cdn资源目录,做资源版本管理用,生成的目录和wxgame同级,为空则直接拷贝res到minigame中 */
            "skipResCheck": false, /** 跳过资源检查,即:不拷贝bin中的素材到指定目录 */
            "skipResHash": true, /**(未实现)跳过资源的hash重命名 */
            "skipCodeUglify": true, /** 跳过代码压缩*/
            "enginePlugin": false, /** (只实现了插件2.3.0)是否使用引擎插件,当前默认使用插件2.3.0 */
            "es6toes5": false /** (未实现)不推荐使用! 代码是否要把es6转到se5, 因为微信支持ios9,laya引擎如果是2.2以后,且准备支持ios9的,需要true */
        },
        "config": { /** 这个config是具体游戏中需要的配置，项目里会直接生成一个config.js文件，挂在window['config']下面 */
            "cdn": "https://xxx.xx.xxx/1.0.0/", /** 注意后面的/ , 这个地方也可以不需要1.0.0, 不过要在项目代码中自定义, (cdn + ver + '/')作为组合 */
            "debug": true, /** 项目中是否显示输出和是否有debug方法 */
            "ver": "1.0.0" /**这个(ver 1.0.0)要和packageOptions中的resVer保持一致*/
        },
        "packageOverScript": "" /** 打包完成后是否需要自定义脚本,分包或者git提交的逻辑一般在这里处理; 注意cmd中不要加pause; 这里的cmd会自动生成oppo_dev.cmd */
    },
    "prod": { /** 正式版本的配置*/
        "packageOptions": {
            "packageId": "com.xx.xx", /** oppo中的manifest.json中的package的名字定义 */
            "gameName": "游戏名字", /** oppo中的manifest.json中的name的名字定义 */
            "versionName": "1.0.0", /** oppo中的manifest.json中的版本号定义，每次打正式的时候都要修改，测试无所谓 */
            "versionCode": 1, /** oppo中的manifest.json中的版本数字定义，每次打正式的时候都要修改，测试无所谓 */
            "minPlatformVersion": 1050, /** oppo中的manifest.json中的最小平台版本号定义 */
            "iconPath": "D:/xxx/logo.png", /** oppo中的manifest.json中的icon定义 */
            "orientation": "portrait", /** oppo中的manifest.json中的横竖屏定义 */
            "signPath": "D:/xxx/sign/release", /** oppo中打正式包需要的签名文件的路径,dev模式下可以为空，prod需要填，签名需要自己下载opensll自己生成 */
            "projectBin": "D:/xxxx/",
            "outputPath": "D:/publish/",
            "resPath": [
                "res",
                "resfirst"
            ],
            "resVer": "1.0.0",
            "skipResCheck": false,
            "skipResHash": true,
            "skipCodeUglify": false,
            "enginePlugin": false,
            "es6toes5": false
        },
        "config": {
            "cdn": "https://xxx.xx.xxx/1.0.0/",
            "debug": false,
            "ver": "1.0.0"
        },
        "packageOverScript": ""
    }
}
```


#### 打包完毕后，会启动一个服务，弹出网页，扫码即可方便的调试vivo的rpk，重新打包需要先将服务关闭
layapp-project-vivo.json
```
{
    "init": {
        "plat": "${PLAT}", /** "web", "qq", "tt", "vivo", ... */
        "templatePath": "", /**不填则调用软件自带的模板*/
        "appid": "", /**即微信或者其他平台的appid*/
        "projectName": "${PROJECT_NAME}" /**项目名*/
    },
    "dev": { /**开发版本的配置*/
        "packageOptions": { /**打包的配置选项*/
            "packageId": "com.xx.xx.vivominigame", /** vivo中的manifest.json中的package的名字定义 */
            "gameName": "游戏名字", /** vivo中的manifest.json中的name的名字定义 */
            "versionName": "1.0.0", /** vivo中的manifest.json中的版本号定义，每次打正式的时候都要修改，测试无所谓 */
            "versionCode": 1, /** vivo中的manifest.json中的版本数字定义，每次打正式的时候都要修改，测试无所谓 */
            "iconPath": "D:/xxx/logo.png", /** vivo中的manifest.json中的icon定义 */
            "minPlatformVersion": "1056", /** vivo中的最小兼容全量版本,建议在1056之后 */
            "orientation": "portrait", /** vivo中的manifest.json中的横竖屏定义，value:landscape|portrait */
            "signPath": "", /** vivo中打正式包需要的签名文件的路径,dev模式下可以为空，prod需要填，签名需要自己下载opensll自己生成 */
            "thirdEngine": {
                "laya": "2.4.0"
            },
            "projectBin": "D:/xxxx/", /** 绝对路径 打包时的来源目录,一般是bin目录*/
            "outputPath": "D:/publish/", /** 绝对路径 导出对应的平台项目目录*/
            "resPath": [
                "res"
            ], /** 资源在bin下的目录名列表,要拷贝到minigame中的 */
            "resVer": "", /**生成cdn资源目录,做资源版本管理用,生成的目录和wxgame同级,为空则直接拷贝res到minigame中 */
            "skipResCheck": false, /** 跳过资源检查,即:不拷贝bin中的素材到指定目录 */
            "skipResHash": true, /**(未实现)跳过资源的hash重命名 */
            "skipCodeUglify": true, /** 跳过代码压缩*/
            "enginePlugin": false, /** (只实现了插件2.3.0)是否使用引擎插件,当前默认使用插件2.3.0 */
            "es6toes5": false /** (未实现)不推荐使用! 代码是否要把es6转到se5, 因为微信支持ios9,laya引擎如果是2.2以后,且准备支持ios9的,需要true */
        },
        "config": { /** 这个config是具体游戏中需要的配置，项目里会直接生成一个config.js文件，挂在window['config']下面 */
            "cdn": "https://xxx.xx.xxx/1.0.0/", /** 注意后面的/ , 这个地方也可以不需要1.0.0, 不过要在项目代码中自定义, (cdn + ver + '/')作为组合 */
            "debug": true, /** 项目中是否显示输出和是否有debug方法 */
            "ver": "1.0.0" /**这个(ver 1.0.0)要和packageOptions中的resVer保持一致*/
        },
        "packageOverScript": "" /** 打包完成后是否需要自定义脚本,分包或者git提交的逻辑一般在这里处理; 注意cmd中不要加pause; 这里的cmd会自动生成vivo_dev.cmd */
    },
    "prod": { /** 正式版本的配置*/
        "packageOptions": {
            "packageId": "com.xx.xx", /** vivo中的manifest.json中的package的名字定义 */
            "gameName": "游戏名字", /** vivo中的manifest.json中的name的名字定义 */
            "versionName": "1.0.0", /** vivo中的manifest.json中的版本号定义，每次打正式的时候都要修改，测试无所谓 */
            "versionCode": 1, /** vivo中的manifest.json中的版本数字定义，每次打正式的时候都要修改，测试无所谓 */
            "iconPath": "D:/xxx/logo.png", /** vivo中的manifest.json中的icon定义 */
            "minPlatformVersion": "1056", /** vivo中的最小兼容全量版本,建议在1056之后 */
            "orientation": "portrait", /** vivo中的manifest.json中的横竖屏定义 */
            "signPath": "D:/xxx/sign/release", /** vivo中打正式包需要的签名文件的路径,dev模式下可以为空，prod需要填，签名需要自己下载opensll自己生成 */
            "thirdEngine": {
                "laya": "2.4.0"
            },
            "projectBin": "D:/xxxx/",
            "outputPath": "D:/publish/",
            "resPath": [
                "res"
            ],
            "resVer": "1.0.0",
            "skipResCheck": false,
            "skipResHash": true,
            "skipCodeUglify": false,
            "enginePlugin": false,
            "es6toes5": false
        },
        "config": {
            "cdn": "https://xxx.xx.xxx/1.0.0/",
            "debug": false,
            "ver": "1.0.0"
        },
        "packageOverScript": ""
    }
}
```

### 合并多个json文件到一个js文件（命令虽然还能用，但是xlsx的转变工具已经集成了这个功能了）

我在xlsx这个工具中，已经集成好了转js的功能，不需要这个了。。。
mjj这个工具,需要使用vscode中插件商店里的: xlsxConvert这个转表插件
或者自己生成的json文件也行，但是必须要有一个_filelist_.json的包含所有需要合并的json名字列表
例如_filelist_.json的内容：
```
["a.json","b.json","c.json",...]
```

合并后，就是:
```
"use strict";
var tableData = {
a:{xxx},
b:{xxx},
...
};
if (window) {
window["tableData"] = tableData;
}
```

```
layapp mjj F:/test/jsoncfg.json
```

#### jsoncfg.json配置详情

```
{
    "plat": "",// 暂时无用
    "projectBasePath": "f:/test",// 这个是合并的基础目录，下面的都是相对目录
    "jsonPath": "./data", // 这个是具体的json文件目录
    "outputUrl": "./tabledata.js",// 这个是合并后的js文件
    "exportName": "tableData"// 这个是window下面挂载的导出json集合的key
}
```


### 创建laya项目（不兼容layaide创建的项目，当然熟悉文件的童鞋可以自己改成兼容）

创建的laya项目，默认是使用fairyGUI来作为UI框架的。fairyGUI是我使用到目前为止，开发最方便的UI框架了。

进入到需要创建的目录中，shift+右键，选择打开PowerShell窗口，或者cmd窗口，输入：
```
layapp cp project_name
```
or
```
layapp cp project_name 2.4.0
```

```
创建项目须知：

1. 安装nodejs v8.9以上 或者 nodejs v10 (判断是否安装，请按组合键：win+r，输入cmd，然后输入node -v，出现版本号即安装好了)

2. 安装layapp工具，在cmd窗口中，输入：npm i layapp -g

3. 判断是否安装完毕layapp，方法是：cmd窗口中，输入：layapp v。出现版本号即安装好了。

4. 在一个你要创建项目的目录中，按住：shift+鼠标右键，选择在此处打开powershell窗口。然后输入：layapp cp project

5. 进入project目录，然后右键client目录，使用vscode打开

6. 打开vscode后，按F5，即可开始调试。如果看到页面上出现开始游戏的按钮，则说明创建一个可以执行的游戏项目的第一步成功了。

第二步

1. 我们做游戏，当然是不能用默认项目里的资源，这里就需要使用到fairygui这个UI制作、编辑软件。

2. 安装fairygui工具。到fairygui官网 https://www.fairygui.com/，下载最新版。

3. 解压fairygui到自己知道的目录，然后进入，打开fairygui.exe，选择打开文件夹。选择自己创建的项目的fguiproject文件夹打开。

10.打开项目后，增加或者修改Home组件中的组件，然后按F9发布

11.回到vscode，按下F5，如果看到页面上出现自己修改的显示，则说明修改已经成功了，恭喜你，第二步你也学会了。
```