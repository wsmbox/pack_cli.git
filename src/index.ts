#! /usr/bin/env node
import { readFileSync } from "fs";
import * as layacp from "layacp";
import { join } from "path";
import { init } from "./init";
import { merge } from "./mergejson2js/MergeJson2js";
import { PackMgr } from "./pack/PackMgr";

const command = process.argv;


switch (command[2]) {
    case 'create':
    case 'cp':
        layacp.createProject(command[3], command[4], command[5]);
        break;

    case 'init':
    case 'i':
        init(command[3], command[4]);
        break;

    case 'dev':
    case 'd':
        PackMgr.inst.pack('dev', command[3]);
        break;

    case 'prod':
    case 'p':
        PackMgr.inst.pack('prod', command[3]);
        break;

    case 'mergejson2js':
    case 'mjj':
        merge(command[3]);
        break;

    case 'tinypng':
    case 'tp':
        break;
    case 'help':
    case 'h':
        showHelpStr();
        break;
    case 'version':
    case 'v':
        let verUrl = join(__dirname, '../package.json');
        let verJson = JSON.parse(readFileSync(verUrl).toString())
        console.log('version:', verJson.version);
        break;
    case 'test':
        // call_openssl()
        break;
    default:
        showHelpStr()
        break;
}

function showHelpStr() {
    console.log("==> thank you for using layapp <==");
    console.log("if you have question, you can tell me. mail: bfstiger@gmail.com");
    console.log("usage:");

    console.log(`create laya project: layapp create myProj 2.4.0 F:/test`);
    console.log(`merge jsonfiles to one js file: layapp mjj F:/test/jsoncfg.json`);
    // console.log(`compress png use tinypng: layapp tp f:/sourceimg f:/output`);
    console.log(`layapp init projectname wx`);
    console.log(`layapp dev wx or layapp dev wx,oppo or layapp d all`);
    console.log(`layapp dev wx or layapp p wx,oppo or layapp p all`);
}