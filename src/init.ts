import { existsSync, readFileSync, writeFileSync } from "fs";
import { join } from "path";

export function init(name = 'project', plat = 'wx') {
    let cfgFn = `layapp-${name}-${plat}.json`;

    let fp = join(__dirname, `../assets/packcfg/pack_${plat}.json`)
    if (!existsSync(fp)) {
        console.log('no such file about init packcfg');
        return;
    }
    let f = readFileSync(fp);
    let docReg = new RegExp(/\/\*\*.*\*\//g);
    let temp = f.toString().replace(docReg, '');
    temp = temp.replace('${PLAT}', plat);
    temp = temp.replace('${PROJECT_NAME}', name);

    writeFileSync(join(process.cwd(), cfgFn), temp);

    console.log('\x1b[92m'+join(process.cwd(), cfgFn), 'has been created!');
    console.log('\x1b[97m'+'please goto ' + process.cwd() + ' to edit ' + cfgFn);
    console.log('use: layapp dev [plat] or layapp prod [plat]')
    
}