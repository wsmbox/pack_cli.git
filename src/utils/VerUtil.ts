/**
 * 对比两个版本大小，v1大则返回1，v1小则返回-1，否则0
 * @param v1 
 * @param v2 
 */
export function compareVersion(vs1: string, vs2: string) {
    console.log("比较版本" + vs1 + ":" + vs2);
    let v1 = vs1.split('.')
    let v2 = vs2.split('.')
    const len = Math.max(v1.length, v2.length)

    while (v1.length < len) {
        v1.push('0')
    }
    while (v2.length < len) {
        v2.push('0')
    }

    for (let i = 0; i < len; i++) {
        const num1 = parseInt(v1[i])
        const num2 = parseInt(v2[i])

        if (num1 > num2) {
            return 1
        } else if (num1 < num2) {
            return -1
        }
    }

    return 0
}