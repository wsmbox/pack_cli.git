export function getTodaytr() {
    let d = new Date();
    let m = d.getMonth() + 1;
    let mstr = m > 9 ? m + '' : '0' + m;
    return `${d.getFullYear()}-${mstr}-${d.getDate()}`;
}