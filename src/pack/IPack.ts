/** 此接口是微信的打包参数 */
export interface IPackageOptions {
    projectBin: string,
    outputPath: string,
    resPath: string[],
    skipResCheck: boolean,
    skipResHash: boolean,
    skipCodeUglify: boolean,
    resVer: string,
    enginePlugin: string,
    es6toes5: boolean
}

export interface IOppoPackageOptions extends IPackageOptions {
    packageId: string;
    gameName: string;
    versionName: string;
    versionCode: number;
    iconPath: string;
    orientation: string;
    signPath: string;
    minPlatformVersion: number;
    thirdEngine?: { [engineName: string]: string };
}

export interface IPack {

    packProject(mode: string): void;

}