import * as zipper from "zip-local";
import { join } from "path";

export function testUnzip(){
    let file = join(_getTemplateUrl(), 'laya-libs.zip');
    console.log('file:',file);
    
    let newUrl = join(process.cwd(), 'test');
    console.log('newUrl:',newUrl);
    
    zipper.sync.unzip(file).save(newUrl);

}

function _getTemplateUrl() {
    return join(__dirname, `../../assets/engineplugin`);
}